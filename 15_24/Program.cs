﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _15_24
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            StreamWriter writer = new StreamWriter("output.txt");

            /* 
            // обычный порядок 

            string str = reader.ReadToEnd();
            writer.WriteLine(str);*/

            // обратный порядок

            string[] text = File.ReadAllLines("input.txt");

             for (int i = text.Length - 1; i >= 0; i--)
             {
                 writer.WriteLine(text[i]);
             } 

            reader.Close();
             writer.Close();
             
            Console.ReadKey();

        }
    }
}
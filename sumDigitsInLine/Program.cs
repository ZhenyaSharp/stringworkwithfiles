﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sumDigitsInLine
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "лтывлоат63420ыволаиол8263выолаил12344олывиол";
            int sum = 0;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] >= '0' && str[i] <= '9')
                {
                    sum += Convert.ToInt32(str[i].ToString());
                }
            }

            Console.WriteLine(str);
            Console.WriteLine("сумма всех цифр= " + sum);

            Console.ReadKey();
        }
    }
}

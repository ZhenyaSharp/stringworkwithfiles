﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _15_29
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            StreamWriter writer = new StreamWriter("outputChet.txt");
            StreamWriter writer2 = new StreamWriter("outputNechet.txt");

            int stringNumber = 0;

            while (!reader.EndOfStream)
            {
                string currentString = reader.ReadLine();
                stringNumber++;

                if (stringNumber % 2 == 0)
                {
                    writer.WriteLine(currentString);
                }
                else
                {
                    writer2.WriteLine(currentString);
                }
            }

            reader.Close();
            writer.Close();
            writer2.Close();
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skobki
{
    class Program
    {
        static void Main(string[] args)
        {
            //дана строка состоящая из скобочек (3+4)*(2-5)+(5-6)+(8+(2-4))

            string str = "(3+4)*(2-5)+(5-6)+(8+(2-4))";
            int numOpen = 0;
            int numClose = 0;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '(')
                {
                    numOpen++;
                }

                if (str[i] == ')')
                {
                    numClose++;
                }

                if (numClose > numOpen)
                {
                    Console.WriteLine("неправильный порядок");
                    break;
                }
            }

            if (numClose != numOpen)
            {
                Console.WriteLine("неправильный порядок");
            }
            else
            {
                Console.WriteLine("скобки раставленны правильно");
            }

            Console.ReadKey();
        }
    }
}


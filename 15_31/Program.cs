﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _15_31
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = new StreamWriter("output.txt");

            string[] text1Array = File.ReadAllLines("input1.txt");
            string[] text2Array = File.ReadAllLines("input2.txt");

            for (int i = 0; i < text2Array.Length - 1; i++)
            {
                if (text1Array[i] != text2Array[i])
                {
                    writer.WriteLine($"отличаются на {i + 1} строке");
                    break;
                }
            }

            writer.Close();
            Console.ReadKey();

        }
    }
}

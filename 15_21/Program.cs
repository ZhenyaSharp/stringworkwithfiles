﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _15_21
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            StreamWriter writer = new StreamWriter("output.txt");
            int stringNumber = 0;

            while (!reader.EndOfStream)
            {
                string currentString = reader.ReadLine();
                stringNumber++;

                if (currentString[0] == 'Т')
                {
                    writer.WriteLine(stringNumber + " строка начинается с Т");
                    break;
                }
            }

            reader.Close();
            writer.Close();
            Console.ReadKey();
        }
    }
}